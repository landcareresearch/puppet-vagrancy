# @summary Installs and manages the vagrancy service.
# @see https://github.com/ryandoyle/vagrancy
#
# @param version
#   The version to install.
#   @see https://github.com/ryandoyle/vagrancy/releases
#
# @param filestore_path
#   The path to the file store.
#   Note, this module manages the directory and ensures its created.
#
class vagrancy (
  String $version        = '0.0.4',
  String $filestore_path = '/opt/vagrancy/data',
){

  # variables
  $root_dir     = '/opt/vagrancy'
  $dl_dir       = "${root_dir}/download"
  $install_path = "${root_dir}/vagrancy-${version}-linux-x86_64"
  $config_path  = "${install_path}/config.yml"
  $filename     = "vagrancy-${version}-linux-x86_64.tar.gz"
  $url          = "https://github.com/ryandoyle/vagrancy/releases/download/${version}/${filename}"
  $service_name = 'vagrancy'
  $service_file = "/etc/systemd/system/${service_name}.service"

  file{[$root_dir,$dl_dir,$filestore_path]:
    ensure => directory,
  }

  archive{'vagrancy':
    path         => "${dl_dir}/${filename}",
    source       => $url,
    extract      => true,
    extract_path => $root_dir,
    require      => File[$root_dir,$dl_dir],
  }

  file{$config_path:
    ensure  => file,
    content => epp('vagrancy/config.yml.epp',{
      filestore_path => $filestore_path,
    }),
    require => Archive['vagrancy'],
  }

  file{$service_file:
    ensure  => file,
    mode    => '0755',
    content => epp('vagrancy/vagrancy.service.epp',{
      install_path => $install_path,
    }),
    require => File[$config_path],
  }

  exec{'update_vagrancy_systemd':
    command     => "/bin/systemctl daemon-reload && /bin/systemctl enable ${service_name}.service",
    refreshonly => true,
    subscribe   => File[$service_file],
  }

  # setup service
  service {'vagrancy':
    ensure    => running,
    subscribe => Exec['update_vagrancy_systemd'],
  }
}
