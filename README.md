
# Vagrancy Puppet Module

[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_PuppetVagrancy_Build%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_PuppetVagrancy_Build&guest=1)

## Overview

Installs and manages the [vagrancy service](https://github.com/ryandoyle/vagrancy).

## API Documentation

See REFERENCE.md
