# vagrancy Puppet Module Changelog

## 2021-05-07 Version 6.0.0

- Updated for Puppet 6 compliance.

## 2019-04-18 Release 0.1.0

- Initial release
